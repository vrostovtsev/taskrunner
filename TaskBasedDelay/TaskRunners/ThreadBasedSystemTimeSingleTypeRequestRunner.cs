﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NodaTime;
using TaskBasedDelay.Implementation;
using TaskBasedDelay.Interface;

namespace TaskBasedDelay.TaskRunners
{
    public class ThreadBasedSystemTimeSingleTypeRequestRunner:RequestRunner
    {
        public ThreadBasedSystemTimeSingleTypeRequestRunner(IRequestFeeder requestFeeder, IReceiptReciever receiptReceiver, IThreadDelayControl threadDelayControl, IRequestDelayControl requestDelayControl, IClock clock) : base(requestFeeder, receiptReceiver, threadDelayControl, requestDelayControl, clock)
        {
        }

        public override void Run()
        {
            RunInternal();
        }

        private void RunInternal()
        {
            do
            {
                try
                {
                    var tasks = RequestFeeder.GetTasks();
                    if (tasks.Any())
                    {
                        foreach (var trTask in tasks)
                        {
                            var taskReceipt = ProcessTask(trTask, RequestDelayControl, Clock);
                            ReceiptReceiver.AddTaskReceipt(taskReceipt);
                        }
                    }
                    else
                    {
                        ThreadDelayControl.OnNoRequestsNewOrRunning();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                ThreadDelayControl.DelayBetweenPollingForTasks();
            } while (ThreadDelayControl.ShouldContinueWorking());
        }

        private static IRequestReceipt ProcessTask(IRequest request, IRequestDelayControl requestDelayControl, IClock clock)
        {
            requestDelayControl.GetDelayTask().Wait();

            var taskReceipt = new RequestReceipt(request, clock.Now.ToDateTimeUtc());
            return taskReceipt;
        }
    }
}
