﻿using System;
using System.Linq;
using System.Threading.Tasks;
using NodaTime;
using TaskBasedDelay.Implementation;
using TaskBasedDelay.Interface;

namespace TaskBasedDelay.TaskRunners
{
    public abstract class RequestRunner
    {
        protected readonly IRequestFeeder RequestFeeder;
        protected readonly IReceiptReciever ReceiptReceiver;
        protected readonly IThreadDelayControl ThreadDelayControl;
        protected readonly IRequestDelayControl RequestDelayControl;
        protected readonly IClock Clock;

        protected RequestRunner(IRequestFeeder requestFeeder, IReceiptReciever receiptReceiver, IThreadDelayControl threadDelayControl, IRequestDelayControl requestDelayControl, IClock clock)
        {
            RequestFeeder = requestFeeder;
            ReceiptReceiver = receiptReceiver;
            ThreadDelayControl = threadDelayControl;
            RequestDelayControl = requestDelayControl;
            Clock = clock;
        }

        public abstract void Run();
    }
}
