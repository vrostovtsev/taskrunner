﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using NodaTime;
using TaskBasedDelay.Implementation;
using TaskBasedDelay.Interface;

namespace TaskBasedDelay.TaskRunners
{
    public class AsyncSystemTimeSingleTypeRequestRunner:RequestRunner
    {
        public AsyncSystemTimeSingleTypeRequestRunner(IRequestFeeder requestFeeder, IReceiptReciever receiptReceiver, IThreadDelayControl threadDelayControl, IRequestDelayControl requestDelayControl, IClock clock) 
            : base(requestFeeder, receiptReceiver, threadDelayControl, requestDelayControl, clock)
        {

        }

        readonly ConcurrentDictionary<Guid, Task<IRequestReceipt>> _requestTasks = new ConcurrentDictionary<Guid, Task<IRequestReceipt>>();
        
        public override void Run()
        {
            RunInternalAsync().Wait();
        }

        private async Task RunInternalAsync()
        {
            do
            {
                try
                {
                    var requests = RequestFeeder.GetTasks();
                    
                    if (requests.Any())
                    {
                        foreach (var trTask in requests)
                        {
                            var taskReceipt = ProcessTask(trTask, RequestDelayControl, Clock);
                            _requestTasks[trTask.TaskUID] = taskReceipt;

                            var receipt = await taskReceipt;
                            ReceiptReceiver.AddTaskReceipt(receipt);

                            Task<IRequestReceipt> t;
                            _requestTasks.TryRemove(trTask.TaskUID, out t);
                        }
                    }
                    else if (_requestTasks.IsEmpty)
                    {
                        ThreadDelayControl.OnNoRequestsNewOrRunning();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                ThreadDelayControl.DelayBetweenPollingForTasks();
            } while (ThreadDelayControl.ShouldContinueWorking());
        }

        private async Task<IRequestReceipt> ProcessTask(IRequest request, IRequestDelayControl requestDelayControl, IClock clock)
        {
            using (await requestDelayControl.TaskMutex.LockAsync())
            {
                var delayTask = requestDelayControl.GetDelayTask();

                var continuationAfterDelay = delayTask.ContinueWith((dtsk) =>
                {
                    var taskReceipt = new RequestReceipt(request, clock.Now.ToDateTimeUtc());
                    return (IRequestReceipt) taskReceipt;
                });

                return await continuationAfterDelay;
            }
        }
    }
}
