﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskBasedDelay.Interface;

namespace TaskBasedDelay.Implementation
{
    public class Request:IRequest
    {
        public Guid TaskUID { get; set; }
    }
}
