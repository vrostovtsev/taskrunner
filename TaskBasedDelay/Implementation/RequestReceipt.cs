﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskBasedDelay.Interface;

namespace TaskBasedDelay.Implementation
{
    public class RequestReceipt :IRequestReceipt
    {
        public Guid TaskUID =>_request.TaskUID;
        public DateTime ProcessedTime { get; private set; }

        private readonly IRequest _request;

        public RequestReceipt(IRequest request, DateTime processedTime)
        {
            _request = request;
            ProcessedTime = processedTime;
        }
    }
}
