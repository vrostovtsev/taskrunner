﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskBasedDelay.Interface
{
    public interface IReceiptReciever
    {
        void AddTaskReceipt(IRequestReceipt receipt);
    }
}
