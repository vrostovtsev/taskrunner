﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TaskBasedDelay.Interface
{
    public interface IRequestDelayControl
    {
        AsyncLock TaskMutex { get; }
        int GetTaskIntervalDelayMs();
        Task GetDelayTask();
    }
}
