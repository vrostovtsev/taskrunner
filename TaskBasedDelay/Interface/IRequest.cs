﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskBasedDelay.Interface
{
    public interface IRequest
    {
        Guid TaskUID { get; set; }
    }
}
