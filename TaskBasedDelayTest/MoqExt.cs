﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Moq;

namespace TaskBasedDelayTest
{
    public static class MoqExtensions
    {
        // Returns sequence, then the last value in the sequence indefinitely
        public static void ReturnSequenceInfiniteEnd<TSource, TRet>(
            this Mock<TSource> sourceMock,
            Expression<Func<TSource, TRet>> mockedFunction,
            params TRet[] returnValuesLastInfinite)
            where TSource : class
        {
            var numCalls = 0;

            sourceMock
                .Setup(mockedFunction)
                .Returns(() => 
                    numCalls < returnValuesLastInfinite.Length 
                        ? returnValuesLastInfinite[numCalls] 
                        : returnValuesLastInfinite.Last())
                .Callback(() => numCalls++);
        }

        public static void ReturnSequenceInfiniteGenerated<TSource, TRet>(
            this Mock<TSource> sourceMock,
            Expression<Func<TSource, TRet>> mockedFunction,
            Func<TRet> returnValueGenerator)
            where TSource : class
        {
            sourceMock
                .Setup(mockedFunction)
                .Returns(returnValueGenerator);
        }
    }
}
