﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using NodaTime;
using NodaTime.Testing;
using NUnit.Framework;
using TaskBasedDelay;
using TaskBasedDelay.Implementation;
using TaskBasedDelay.Interface;
using TaskBasedDelay.TaskRunners;

namespace TaskBasedDelayTest
{
    public class RequestRunnerTest
    {
        protected Mock<IRequestFeeder> RequestFeeder;
        protected Mock<IReceiptReciever> ReceiptReceiver;
        protected Mock<IThreadDelayControl> ThreadDelayControl;
        protected Mock<IRequestDelayControl> RequestDelayControl;

        protected IClock TestClock;
        protected ManualResetEvent Mre;
        protected AsyncLock TaskLockObject;

        [SetUp]
        public void Setup()
        {
            Mre = new ManualResetEvent(false);

            ThreadDelayControl = new Mock<IThreadDelayControl>(MockBehavior.Strict);
            ThreadDelayControl.Setup(tdc => tdc.DelayBetweenPollingForTasks());
            ThreadDelayControl.Setup(tdc => tdc.OnNoRequestsNewOrRunning()).Callback(() =>
            {
                Mre.Set();
            });

            ThreadDelayControl.Setup(tdc => tdc.ShouldContinueWorking()).Returns(
                () =>
                {
                    var resetEventWaitResult = !Mre.WaitOne(10);
                    return resetEventWaitResult;
                });
            
            RequestDelayControl = new Mock<IRequestDelayControl>(MockBehavior.Strict);

            TaskLockObject = new AsyncLock();
            const int constIntervalDelayMs = 200;
            RequestDelayControl.SetupGet(tdc => tdc.TaskMutex).Returns(TaskLockObject);

            // TestClock = SystemClock.Instance; 
            TestClock = new FakeClock(SystemClock.Instance.Now);

            ConfigureDelayTaskDelayControl(TestClock, ()=>constIntervalDelayMs);
            
            RequestFeeder = new Mock<IRequestFeeder>(MockBehavior.Strict);
            ReceiptReceiver = new Mock<IReceiptReciever>(MockBehavior.Strict);
        }

        private void ConfigureDelayTaskDelayControl(IClock testClock, Func<int> intervalDelayFunc)
        {
            RequestDelayControl.ReturnSequenceInfiniteGenerated(tdc => tdc.GetTaskIntervalDelayMs(), intervalDelayFunc);
            var fClock = testClock as FakeClock;
            if (fClock != null)
            {
                RequestDelayControl.ReturnSequenceInfiniteGenerated(tdc => tdc.GetDelayTask(), () => Task.Delay(10).ContinueWith((t) => fClock.AdvanceMilliseconds(intervalDelayFunc())));
            }
            else
            {
                RequestDelayControl.ReturnSequenceInfiniteGenerated(tdc => tdc.GetDelayTask(), () => Task.Delay(intervalDelayFunc()));
            }
        }

        public void TaskRunner_Runs1Task(RequestRunner runner)
        {
            var taskList = new List<IRequest>
            {
                new Request { TaskUID = Guid.NewGuid(), },
            };

            RequestFeeder.ReturnSequenceInfiniteEnd(tf => tf.GetTasks(), taskList, new List<IRequest>());
            
            var receiptList = new ConcurrentBag<IRequestReceipt>();
            ReceiptReceiver.Setup(tr => 
              tr.AddTaskReceipt(It.IsAny<IRequestReceipt>()))
                .Callback<IRequestReceipt>(ttr => receiptList.Add(ttr));

            runner.Run();

            Assert.That(receiptList.Any());
            Assert.That(receiptList.Single().TaskUID == taskList.Single().TaskUID);
        }

        public void TaskRunner_Runs2Tasks(RequestRunner runner)
        {
            var taskList = new List<IRequest>
            {
                new Request { TaskUID = Guid.NewGuid(), },
                new Request { TaskUID = Guid.NewGuid(), },
            };

            RequestFeeder.ReturnSequenceInfiniteEnd(tf => tf.GetTasks(), taskList, new List<IRequest>());

            var receiptList = new ConcurrentBag<IRequestReceipt>();
            ReceiptReceiver.Setup(tr =>
              tr.AddTaskReceipt(It.IsAny<IRequestReceipt>()))
                .Callback<IRequestReceipt>(ttr => receiptList.Add(ttr));

            runner.Run();

            Assert.That(receiptList.Any());
            var taskGuids = taskList.Select(x => x.TaskUID);
            var receiptGuid = receiptList.Select(x => x.TaskUID);

            CollectionAssert.AreEquivalent(taskGuids, receiptGuid);
            var sortedByReceiptTime = receiptList.OrderBy(rc => rc.ProcessedTime).ToList();
            AssertIntervalTime(sortedByReceiptTime, RequestDelayControl.Object.GetTaskIntervalDelayMs());
        }

        public void TaskRunner_Runs4TasksVariableInterval(RequestRunner runner)
        {
            const int lowIntervalDelayMs = 200;
            var taskListLowInterval = new List<IRequest>
            {
                new Request { TaskUID = Guid.NewGuid(), },
                new Request { TaskUID = Guid.NewGuid(), },
            };

            const int highIntervalDelayMs = 400;
            var taskListHighIntervalDelay = new List<IRequest>
            {
                new Request { TaskUID = Guid.NewGuid(), },
                new Request { TaskUID = Guid.NewGuid(), },
            };

            // below is a messy implementation of state machine that switches (using switchTriggered) 
            // between serving (lowIntervalDelayMS, taskListLowInterval) to (highIntervalDelayMs, taskListHighIntervalDelay) 
            // switch is triggered by the first OnNoRequestsNewOrRunning() call, aka "when first tasks are processed completely".

            bool firstGiven = false;
            bool secondGiven = false;
            bool switchTriggered = false;

            Func<List<IRequest>> fnReq = () =>
            {
                if (!switchTriggered)
                {
                    if (!firstGiven)
                    {
                        firstGiven = true;
                        return taskListLowInterval;
                    }
                    else
                    {
                        return new List<IRequest>();
                    }
                }
                else
                {
                    if (!secondGiven)
                    {
                        secondGiven = true;
                        return taskListHighIntervalDelay;
                    }
                    else
                    {
                        return new List<IRequest>();
                    }
                }
            };

            int interleaveDelayMs = lowIntervalDelayMs;
            ConfigureDelayTaskDelayControl(TestClock, () => interleaveDelayMs);

            RequestFeeder.ReturnSequenceInfiniteGenerated(tf => tf.GetTasks(), fnReq);

            ThreadDelayControl.Setup(tdc => tdc.OnNoRequestsNewOrRunning()).Callback(() =>
            {
                if (!switchTriggered)
                {
                    switchTriggered = true;
                    interleaveDelayMs = highIntervalDelayMs;
                }
                else
                {
                    Mre.Set();
                }
            });

            // messy state machine ends

            var receiptList = new ConcurrentBag<IRequestReceipt>();
            ReceiptReceiver.Setup(tr =>
              tr.AddTaskReceipt(It.IsAny<IRequestReceipt>()))
                .Callback<IRequestReceipt>(ttr => receiptList.Add(ttr));

            runner.Run();

            Assert.That(receiptList.Any());
            var taskGuids = taskListLowInterval.Select(x => x.TaskUID).Union(taskListHighIntervalDelay.Select(x => x.TaskUID));
            var receiptGuid = receiptList.Select(x => x.TaskUID);

            CollectionAssert.AreEquivalent(taskGuids, receiptGuid);
            var sortedByReceiptTime = receiptList.OrderBy(rc => rc.ProcessedTime).ToList();

            AssertIntervalTime(sortedByReceiptTime.Take(taskListLowInterval.Count).ToList(), lowIntervalDelayMs);
            AssertIntervalTime(sortedByReceiptTime.Skip(taskListLowInterval.Count).ToList(), highIntervalDelayMs);
        }

        private void AssertIntervalTime(List<IRequestReceipt> sortedByReceiptTime, int getTaskInterleaveDelayMs)
        {
            for (int i = 1; i < sortedByReceiptTime.Count; i++)
            {
                Assert.That((sortedByReceiptTime[i].ProcessedTime - sortedByReceiptTime[i - 1].ProcessedTime).TotalMilliseconds >= getTaskInterleaveDelayMs);
            }
        }
    }
}
