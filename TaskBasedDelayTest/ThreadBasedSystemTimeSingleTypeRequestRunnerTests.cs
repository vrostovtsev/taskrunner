﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TaskBasedDelay.TaskRunners;

namespace TaskBasedDelayTest
{
    [TestFixture]
    public class ThreadBasedSystemTimeSingleTypeRequestRunnerTests : RequestRunnerTest
    {
        [Test]
        public void ThreadBasedSystemTimeSingleTypeTaskRunner_Runs1Task()
        {
            var runner = GetThreadBasedSystemTimeSingleTypeRequestRunner();
            TaskRunner_Runs1Task(runner);
        }
        
        [Test]
        public void ThreadBasedSystemTimeSingleTypeTaskRunner_Runs2Tasks()
        {
            var runner = GetThreadBasedSystemTimeSingleTypeRequestRunner();
            TaskRunner_Runs2Tasks(runner);
        }

        [Test]
        public void ThreadBasedSystemTimeSingleTypeTaskRunner_Runs4TasksVariableInterval()
        {
            var runner = GetThreadBasedSystemTimeSingleTypeRequestRunner();
            TaskRunner_Runs4TasksVariableInterval(runner);
        }

        private ThreadBasedSystemTimeSingleTypeRequestRunner GetThreadBasedSystemTimeSingleTypeRequestRunner()
        {
            var runner = new ThreadBasedSystemTimeSingleTypeRequestRunner(RequestFeeder.Object, ReceiptReceiver.Object, ThreadDelayControl.Object, RequestDelayControl.Object, TestClock);
            return runner;
        }
    }
}
