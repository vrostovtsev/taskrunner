﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TaskBasedDelay.TaskRunners;

namespace TaskBasedDelayTest
{
    [TestFixture]
    public class AsyncSystemTimeSingleTypeRequestRunnerTest:RequestRunnerTest
    {
        [Test]
        public void ThreadBasedSystemTimeSingleTypeTaskRunner_Runs1Task()
        {
            var runner = GetAsyncSystemTimeSingleTypeRequestRunner();
            TaskRunner_Runs1Task(runner);
        }
        
        [Test]
        public void ThreadBasedSystemTimeSingleTypeTaskRunner_Runs2Tasks()
        {
            var runner = GetAsyncSystemTimeSingleTypeRequestRunner();
            TaskRunner_Runs2Tasks(runner);
        }

        [Test]
        public void ThreadBasedSystemTimeSingleTypeTaskRunner_Runs4TasksVariableInterval()
        {
            var runner = GetAsyncSystemTimeSingleTypeRequestRunner();
            TaskRunner_Runs4TasksVariableInterval(runner);
        }

        private AsyncSystemTimeSingleTypeRequestRunner GetAsyncSystemTimeSingleTypeRequestRunner()
        {
            var runner = new AsyncSystemTimeSingleTypeRequestRunner(RequestFeeder.Object, ReceiptReceiver.Object, ThreadDelayControl.Object, RequestDelayControl.Object, TestClock);
            return runner;
        }
    }
}
